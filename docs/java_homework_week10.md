# 教材学习内容总结
## 第39章要点
### 要点1：SharedPreference
* 在Android中我们通常使用 一个轻量级的存储类——SharedPreferences来保存用户偏好的参数。  
* android.content.SharedPreferences接口提供了用于**排序**和**读取**应用程序设置的方法。   
* 通过调用PreferenceManager的getDefaultSharedPreferences静态方法，传入一个Content，以获取SharedPreferences的默认实例。   
* SharedPreferences使用xml文件, 类似于Map集合,使用键-值的形式来存储数据，调用SharedPreferences的getXxx(name)即可根据键获得对应的值。  
* 在Device File Expoler打开data/data/<包名>可以看到在shared_prefs目录下生成了一个xml文件：   
![](https://img2018.cnblogs.com/blog/1511102/201905/1511102-20190503132253860-1533489662.png)


* SharedPreferences使用流程图：[参考](https://www.runoob.com/w3cnote/android-tutorial-sharedpreferences.html)   
![](https://img2018.cnblogs.com/blog/1511102/201905/1511102-20190503132249383-65248097.jpg)


## 第40章要点
### 要点1：存储区域  
* 内部存储：对于应用程序来说是私有的，用户和其他应用程序都不能访问它。   
* 外部存储：存储的文件将会和其他的应用程序分享，其他用户也能够访问外部存储。例如，内建的Camera应用程序将数字图像文件存储在外部存储中，以便用户能够很容易地将其复制到计算机中。  

### 要点2：内部存储
* 内部存储的位置是<code_s>/data/data/\<包名></code_s>。   
* Context类提供了各种方法，可用来从应用程序访问内部存储。应该使用这些方法来访问在内部存储中存储的文件，而且不应该将内部存储的位置直接编写到代码中。   
* Android文件操作模式：[参考](https://www.runoob.com/w3cnote/android-tutorial-file.html)      
![](https://img2018.cnblogs.com/blog/1511102/201905/1511102-20190503133928355-2071750380.jpg)


* Android文件操作方法：[参考](https://www.runoob.com/w3cnote/android-tutorial-file.html)      
![](https://img2018.cnblogs.com/blog/1511102/201905/1511102-20190503133934911-2048022606.jpg)


* 读取SD卡上的文件：[参考](https://www.runoob.com/w3cnote/android-tutorial-file.html)   
![](https://img2018.cnblogs.com/blog/1511102/201905/1511102-20190503133942280-1190224850.jpg)

### 要点3：外部存储 
* 读/写外部存储需要在AndroidManifest.xml中添加如下内容：  

	    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
	    <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />

## 第41章要点
### 要点1：SQLite   
* Android系统已经集成了SQLite数据库，所以无需安装数据库软件。   
* SQlite通过文件来保存数据库，一个文件就是一个数据库，数据库中又包含多个表格，表格里又有多条记录，每个记录由多个字段构成，每个字段有对应的值，每个值我们可以指定类型，也可以不指定类型(主键除外)。  
* Android内置的SQLite是SQLite 3版本的。   
* 使用数据库时会用到的三个类：  
	* SQLiteOpenHelper：抽象类，通过继承该类，重写数据库创建以及更新的方法，还可以通过该类的对象获得数据库实例，或者关闭数据库。  
    * SQLiteDatabase：数据库访问类，可以通过该类的对象来对数据库做一些增删改查的操作。  
    * Cursor：游标，有点类似于JDBC里的resultset结果集，可以简单理解为指向数据库中某一个记录的指针。   

### 要点2：查看db文件   
* 生成的数据库db文件在<code_s>/data/data/<包名>/databases/</code_s>下：   
![](https://img2018.cnblogs.com/blog/1511102/201905/1511102-20190503141107058-407244221.png)

* 在Device File Explorer里是打不开db文件的，可以用SQLite图形化工具（如SQLite Expert Professional）来查看，或者配置adb环境变量后，通过adb shell来查看。   

## 第42章要点
### 要点1：相机 
* 要使用相机，需要在AndroidManifest.xml中添加如下内容：   
	
		<uses-feature android:name="android.hardware.camera"/>  
		<uses-permission android:name="androud.permission.CAMERA"/>

* 如果Camera不符合需要，也可以直接使用Camera API，它允许配置相机的很多参数。   
* Camera API以android.hardware.Camera类为中心。       


# 代码调试中的问题和解决过程
- 问题1：  
Android Studio：xxx is not an enclosing class   
     
- 问题1解决方案：    
	若要创建内部类的实例，需要有外部类的实例才行，或者是将内部类设置为静态的，添加 static 关键字

			public class A{
				public class B{

				}
			}

	如果写成 A.B ab = new A.B()就会出现xxx is not an enclosing class，应该改为

			A a = new A();  
			A.B ab = a.new B(); 

	或将class B 改成static class B 这样就可以直接用A.B ab = new A.B();了。

# [代码托管](https://gitee.com/PNIDEMOOO/zhy20189217)
![](https://img2018.cnblogs.com/blog/1511102/201905/1511102-20190503143356978-2103382503.png)   

# 学习进度条

|            | 代码行数（新增/累积）| 博客量（新增/累积）|学习时间（新增/累积）|重要成长|
| --------   | :----------------:|:----------------:|:---------------:  |:-----:|
| 目标        | 5000行            |   30篇           | 400小时            |       |
| 第一周      | 200/200           |   2/2            | 20/20             |       |
| 第二周      | 300/500           |   1/3            | 17/37             |       |
| 第三周      | 787/1287          |   1/4            | 15/52             |       |
| 第四周      | 350/1637          |   1/5            | 16/68             |       |    
| 第五周      | 1049/2686         |   1/6            | 20/88             |       |   
| 第六周      | 1500/4186         |   1/7            | 18/106            |       |
| 第七周      | 1718/5904         |   1/8            | 14/120            |       |
| 第八周      | 200/6104          |   1/9            | 12/132            |       |
| 第九周      | 800/6904          |   2/11           | 12/144            |       |
| 第十周      | 800/7704          |   1/12           | 15/159            |       |

- 计划学习时间:18小时

- 实际学习时间:15小时


# 参考资料

-  [Java和Android开发学习指南（第二版）](https://book.douban.com/subject/26986744/)   
-  [6.2 数据存储与访问之——SharedPreferences保存用户偏好参数](https://www.runoob.com/w3cnote/android-tutorial-sharedpreferences.html)   
-  [6.1 数据存储与访问之——文件存储读写](https://www.runoob.com/w3cnote/android-tutorial-file.html)   
-  [6.3.1 数据存储与访问之——初见SQLite数据库](https://www.runoob.com/w3cnote/android-tutorial-sqlite-intro.html)  
-  [Android Studio：xxx is not an enclosing class](https://blog.csdn.net/qq_37016828/article/details/78464987)