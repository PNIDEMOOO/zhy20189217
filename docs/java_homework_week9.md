# 教材学习内容总结
## 第35章要点
### 要点1：硬件加速   
* Android APILevel14及其以上版本为目标的应用程序来说，硬件加速是默认可用的。   
* 可通过android:hardwareAccelerated="false"来关闭活动或应用的硬件加速。    

### 要点2：创建一个定制视图   
* 要创建一个定制视图，需要扩展android.view.View类或其一个子类，并且覆盖其onDraw方法。
* onDraw方法的签名为：    
	
		protected void onDraw (android.graphics.Canvas canvas)
 
* 可以使用Canvas中的方法来绘制图形和文本，也可以创建路径和区域来绘制更多的形状。   
* Canvas中的大多数的绘制方法都需要一个Paint。应该在类级别创建Paint，并让可以在onDraw方法中使用，而不是在onDraw方法中创建Canvas。   


## 第36章要点
### 要点1：片段
* 片段是能够嵌入到活动中的组件。   
* 一个片段可以有也可以没有用户界面。   

### 要点2：片段的生命周期  
![](https://img2018.cnblogs.com/blog/1511102/201904/1511102-20190427215604944-1955441058.jpg)   

* onAttach。在片段与其活动关联之后就调用。   
* onCreate。初次创建片段的时候调用。   
* onCreateView。当为片段创建布局的时候调用。它必须返回片段的根视图。  
* onActivityCreated。调用来告诉片段，其活动的onCreate方法已经完成。   
* onStart。当片段的视图对用户可见的时候调用。  
* onResume。当包含的活动暂停的时候，调用该方法。 
* onPause。当包含活动暂停的时候调用。  
* onStop。当包含活动停止的时候调用。   
* onDestroyView。调用以允许片段进行最后的清理工作。    
* onDestroy。在片段销毁之前调用，以允许片段进行最后的清理工作。   
* onDetach。当片段与其活动解除关联的时候调用。  

>如果需要监听一个片段中发生的事件，而它可能影响到活动或其他视图或片段，不要在片段类中编写监听器，而是触发一个新的事件作为对片段事件的响应，并且让活动来处理它。   

### 要点3：Fragment与Activity交互
![](https://img2018.cnblogs.com/blog/1511102/201904/1511102-20190427215616553-1741603621.jpg)   

## 第37章要点
### 要点1：多面板布局  
* 在单面板布局【e.g.手机】中，显式地活动通常包含单个的片段，这个片段往往反过来包含一个ListView，选中ListView上的一项，将会开启另一个活动。   
* 在多面板布局【e.g.平板电脑】中，通常有一个足够大容纳两个面板的活动。可以使用相同的片段，但是这一次，当选中一个项的时候，它将会更新第2个**片段**，而不是开始另一个活动。   

## 第38章要点
### 要点1：属性动画  
* 属性动画背后的动力就是android.animation.Animator类，这是一个抽象类，要使用其子类（ValueAnimator或ObjectAnimator）来创建动画。此外，AnimatorSet类是Animator的另一个子类，设计用来以并行或连续的方式运行多个动画。   

### 要点2：Animator类  
* Animator有一个方法用来设置要进行动画的目标对象（setTarget），还有一个方法用来设置时长（setDuration），还有一个方法用来启动动画（start）。可以在一个Animator对象上多次调用start方法。  
* Animator提供了一个addListener方法，它接受一个Animator.AnimatorListener实例。AnimatorListener接口定义于Animator类中，并且提供当特定的事件发生的时候系统所调用的方法。如果想要相应某一个事件的话，可以实现这些方法中的一个。    
	
	* void onAnimationStart(Animator animation);   
	* void onAnimationEnd(Animator animation);   
	* void onAnimationCancel(Animator animation);    
	* void onAnimationRepeat(Animator animation);    

# 教材学习中的问题和解决过程
- 问题1：   
Fragment的管理中getFragmentManager和getSupportFragmentManager的使用区别？   
     
- 问题1解决方案：         
	* app包下FragmentManager用Fragmentmanager  fragmentManager=getFragmentManager();
	* v-4包的FragmentManager用FragmentManager  fragmentManager=getSupportFragmentManager() 获取

# 代码调试中的问题和解决过程
- 问题1：   
setOnItemClickListener(this)时this报错。   
     
- 问题1解决方案：    
主类中要implements OnItemClickListener。      

# [代码托管](https://gitee.com/PNIDEMOOO/zhy20189217)
![](https://img2018.cnblogs.com/blog/1511102/201904/1511102-20190427230836260-590627424.png)

# 上周考试错题总结
-  错题1：   
Multi-dimensional arrays that contain arrays of different lengths in any one dimension are called ?.（包括不同长度数组的多维数组叫做?）   
A.ragged arrays（锯齿状数组）   
B.static arrays（静态数组）   
C.two-dimensional arrays（二维数组）   
D.constant arrays（常量数组）   
E.overloaded arrays（重载数组）       
正确答案：A    

# 学习进度条

|            | 代码行数（新增/累积）| 博客量（新增/累积）|学习时间（新增/累积）|重要成长|
| --------   | :----------------:|:----------------:|:---------------:  |:-----:|
| 目标        | 5000行            |   30篇           | 400小时            |       |
| 第一周      | 200/200           |   2/2            | 20/20             |       |
| 第二周      | 300/500           |   1/3            | 17/37             |       |
| 第三周      | 787/1287          |   1/4            | 15/52             |       |
| 第四周      | 350/1637          |   1/5            | 16/68             |       |    
| 第五周      | 1049/2686         |   1/6            | 20/88             |       |   
| 第六周      | 1500/4186         |   1/7            | 18/106            |       |
| 第七周      | 1718/5904         |   1/8            | 14/120            |       |
| 第八周      | 200/6104          |   1/9            | 12/132            |       |
| 第九周      | 800/6904          |   2/11           | 12/144            |       |

- 计划学习时间:18小时

- 实际学习时间:12小时


# 参考资料

-  [Java和Android开发学习指南（第二版）](https://book.douban.com/subject/26986744/)   
-  [5.1 Fragment基本概述](https://www.runoob.com/w3cnote/android-tutorial-fragment-base.html)   
-  [android开发学习 ------- 关于getSupportFragmentManager()不可用的问题](https://www.cnblogs.com/mengxiao/p/8477319.html)