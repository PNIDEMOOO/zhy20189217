# 教材学习内容总结
## 第五章要点：
### 要点1：java.lang.Object  
java.lang.Object类表示一个Java对象，所有的类都直接或间接地派生自这个类。
### 要点2：java.lang.String  
#### 两种创建String对象的方式
若创建String对象的方式为将字符串字面值赋值给一个引用变量：

	String s1 = "java";
	String s2 = "java";
	System.out.println(s1==s2);//输出true

这里s1、s2得到的是一个相同的String对象，并且，这个对象不总是新的。如果“java”之前已经创建了，该对象可能来自于一个池。   
  
若创建String对象的方式为使用`new`关键字：

	String s1 = new String("java");
	String s2 = new String("java");
	System.out.println(s1==s2);//输出false

这里的s1、s2创建的是两个不同的实例。   
   
> 使用字符串字面值要更好一些，因为这可使JVM节省了一些本来需要用来构建新的实例的CPU周期。 

#### ==操作符和.equals的区别  
==操作符比较的是操作符两边变量的地址，.equals比较的是两个对象的值是否相同。  


### 要点3：字符串字面值拼接
	String s1 = 12 + 34 + "";//"46"   
	String s2 = "" + 12 + 34;//"1234"  
	String s3 = 12 + "34";//"1234"

顺序：从左往右   

### 要点4：java.lang.Class 
每次JVM创建一个对象时，也创建一个java.lang.Class对象来描述该对象的类型。同一个类的所有实例，都共享同一个Class对象。   

## 第六章要点：
### 要点1：创建数组
	int[] ints = new int[4];//声明和创建一个数组

### 要点2：遍历数组
使用for循环和数组的索引的方式：    

	for (int i = 0;i < 3; i++){
		System.out.println("\t- "+names[i]);
	}

使用增强的for语句：   

	for (String name : names){
		System.out.println(name);
	}

### 要点3：java.util.Arrays 
Arrays类提供了操作数组的静态方法。  

一旦创建数组，不能修改其大小，如果要修改大小，必须创建一个新的数组，并且使用旧的数组值填充它。   
可以用`java.util.Arrays.copyOf()`或`System.copyOfRange()`

## 第八章要点：
### 要点1：try...catch...finally   
* 将可能导致一个错误的代码隔离到一个`try`语句块中；
* 对于每一个单个的`catch`语句块，编写出如果`try`语句块中发生特定类型的一个异常，将要执行的代码；   
* 在`finally`语句块中，编写出不管是否发生错误都将运行的代码。  

> catch和finally语句是可选的，但是，其中至少有一个要存在。 
catch语句块可以有多个。  
   
  
有的情况下没有`catch`只有`try`，例如，在打开一个数据库连接之后，想要确保在操作完数据之后会调用该连接的close方法。   

### 要点2：java.lang.Exception 
* 所有的Java异常类都派生自java.lang.Exception类。   
* 不能将处理java.lang.Exception的一个catch语句块放在娶她catch块之前，因为它用来捕获一切异常，因此，如果放在前边，将会导致后边的catch语句块无法执行。    

### 要点3：throw关键字
* 在方法中捕获异常使用关键字`throw`，把异常抛回给调用者，让调用者来处理它。
* `throws`语句用于在一个方法签名的最后，表示该方法可能会抛出给定类型的异常。    

## 第九章要点：
### 要点1：装箱和拆箱
#### 装箱(boxing)
将基本类型转换为一个包装类对象，叫装箱。   

	Integer number =3;

#### 拆箱(unboxing)
将一个包装类对象转换成基本类型，叫拆箱。   

	Integer number = new Integer(100);
	int simpleNumber = number;

或直接写

	int simpNumber = new Integer(100);

### 要点2：生成随机数
java.lang.Math类的random方法返回0.0到1.0之间的一个double类型的值。

### 要点3：数字解析
数字解析的目的是将一个字符串转换成为一个数字基本类型。   

# 教材学习中的问题和解决过程
- 问题1：toString()默认返回的到底是什么？    
- 问题1解决方案：将Score类中重写的toString()注释掉看看引用同一个对象和不同对象返回的toString()有什么不同，得出结论：返回的是反映这个对象的字符串，同一个对象返回的相同，不同对象返回的不同。
![](https://img2018.cnblogs.com/blog/1511102/201903/1511102-20190317201758920-251824913.png)



# 代码调试中的问题和解决过程
- 问题1：import org.junit.test;时报错cannot resolve symbol "junit"     
- 问题1解决方案：add junit4 to classpath.   
![](https://img2018.cnblogs.com/blog/1511102/201903/1511102-20190317201753442-1134737999.png)


   
   
# [代码托管](https://gitee.com/PNIDEMOOO/zhy20189217)
![](https://img2018.cnblogs.com/blog/1511102/201903/1511102-20190317201739181-1136795203.png)



# 上周考试错题总结
- 错题1   

		String[] strs=new String[5];   

产生0个实例，此语句只是定义了一个元素类型为String且大小为5的数组，但并没有创建String实例。 

- 错题2

		String[] strs = {"Java", "Java", "Java", "Java", "Java"};

此语句虽然初始化了5个String元素，但均由字符串字面值直接赋值创建，这5个元素字符串字面值相同，因此只创建了1个实例。


# 学习进度条

|            | 代码行数（新增/累积）| 博客量（新增/累积）|学习时间（新增/累积）|重要成长|
| --------   | :----------------:|:----------------:|:---------------:  |:-----:|
| 目标        | 5000行            |   30篇           | 400小时            |       |
| 第一周      | 200/200           |   2/2            | 20/20             |       |
| 第二周      | 300/500           |   1/3            | 17/37             |       |
| 第三周      | 787/1287          |   1/4            | 15/52             |       |


- 计划学习时间:18小时

- 实际学习时间:15小时


# 参考资料

-  [Java和Android开发学习指南（第二版）](https://book.douban.com/subject/26986744/)