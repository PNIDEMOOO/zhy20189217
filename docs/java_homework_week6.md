# 教材学习内容总结
## 第23章要点：
### 要点1：Android应用程序组件
有4种Android应用程序组件： 

* 活动（Activity）：包含用户交互组件的一个窗口    
* 服务（Service）：在后台长时间运行的操作    
* 广播接收者（Broadcast receiver）：一个监听器，负责对系统或应用程序声明做出响应    
* 内容提供者（Content provider）：管理要和其他应用程序分享的一组数据的一个组件    


### 要点2：意图（intent）
* 意图是一条消息，发送给系统或另一个应用程序，以要求执行一个动作。   
* 使用意图可以做很多不同的事，但通常是用意图来启动一个活动，启动一个服务或者发送一条广播。   

## 第24章要点：    
### 要点1： Android Studio应用程序结构 
* Project 
	* app：应用程序所有的组件   
		* menifests  
			* AndroidManifest.xml：Android清单，用来描述应用程序    
		* java：包含了所有的Java应用程序和测试类    
		* res：资源文件   
			* drawable：各种屏幕分辨率的图像    
			* layout：布局文件   
			* menu：菜单文件  
			* mipmap：各种屏幕分辨率的app图标    
			* values：字符串和其他值    
	* Gradle Scripts：Gradle构件脚本，供Android Studio构件项目  


### 要点2：R类  
* Android Studio中所看不到的，是一个名为R的通用的Java类，可以在项目的<code_s>app/build/generated/source</code_s>目录下找到它。    
* R的作用是让你能够引用代码中的一个资源，例如可以使用<code_s>R.drawable.logo</code_s>来引用<code_s>res/drawable</code_s>目录下添加的logo.png图像文件。    

## 第25章要点：   
### 要点1：活动的生命周期   
![](https://img2018.cnblogs.com/blog/1511102/201904/1511102-20190406113223547-1412504122.jpg)


Activity类中定义了7个回调方法，覆盖了Activity生命周期的每一个环节。   

* onCreate()   
这个方法在每一个Activity类都会有，当我们新建一个Activity类时，一定会重写父类的onCreate方法，onCreate方法会在Activity第一次被创建时调用。我们应该在这个方法中完成Activity的初始化操作，比如说加载布局，初始化布局控件，绑定按钮事件等。

* onStart()
这个方法在Activity由不可见变为可见时调用。

* onResume()
这个方法在Activity准备好喝用户交互的时候调用。此时的Activity一定位于返回栈的栈顶，并且处于运行状态。

* onPause()
这个方法在系统准备去启动或者恢复另一个Activity的时候调用。

* onStop()
这个方法在Activity完全不可见的时候调用。它和onPause()方法的主要区别在于，如果启动的新Activity是一个对话框式的activity，那么，onPause()方法会得到执行，而onStop()方法并不会执行。

* onDestory()
这个方法在Activity被销毁之前调用，之后Activity的状态将变为销毁状态。

* onRestart()
这个方法在Activity由停止状态变为运行状态之前调用，也就是Activity被重新启动了。

### 要点2：
* 任何活动activity都必须在AndroidManifest中注册之后才能生效。    
* 另外需要注意，如果你的应用程序中没有声明任何一个活动作为主活动，这个程序仍然是可以正常安装的，只是你无法在启动器中看到或者打开这个程序。这种一般都是作为第三方服务供其他的应用在内部进行调用的，如支付宝快捷支付服务。    

### 要点3：Intent
* 通常，要给调用的活动传递额外的信息，可以通过意图附加信息来实现。   
* 通过传递给意图一个活动类而构造的意图，叫作显式意图。    
* 隐式意图没有指定一个意图类，而是给Intent类的构造方法传递一个动作，并且让系统来决定启动哪一个活动。如果有多个活动可以处理该意图，系统通常会让用户来选择。   
* 每个Intent只能制定一个action，却能指定多个category。   

## 第26章要点：   
### 要点1：Toast   
* Toast是一个小的弹出对话框，用来显示一条消息作为给用户的反馈。     
* 和Dialog不一样的是，它永远不会获得焦点，无法被点击。
* Toast类的思想就是尽可能不引人注意，同时还向用户显示信息，希望他们看到。
* Toast显示的时间有限，Toast会根据用户设置的显示时间后自动消失。

### 要点2：inflater   
* LayoutInflater类的作用类似于findViewById(),不同点是LayoutInflater是用来找res/layout/下的xml布局文件，并且实例化；而findViewById()是找xml布局文件下的具体widget控件(如 Button、TextView等)。 
* 具体作用：
	* 对于一个没有被载入或者想要动态载入的界面，都需要使用LayoutInflater.inflate()来载入；   
	* 对于一个已经载入的界面，就可以使用Activiyt.findViewById()方法来获得其中的界面元素。   

### 要点3：AlertDiolog   
* 与Toast相似，AlertDiolog也是一个为用户提供反馈的窗口。   
* Toast能淡出自己，但AlertDiolog会一直显示，直到其失去焦点。    
* 一个AlertDialog最多可以包含3个按钮和一个可选项目的列表。   

### 要点4：通知  
* 通知是出现在状态栏的一条消息。   
* 和Toast不同，通知是持久的，并且将保持显示，直到关闭它或者关闭设备。       

# 教材学习中的问题和解决过程
- 问题1：《Java和Android开发学习指南》（第2版）出版于2016年3月，那时Java8还没有得到正式的支持，Android的版本也才到5.1，Android Studio的版本也和现在不一样，所以教材中有许多代码、说明等在现在的环境上实验会遇到许多错误。                   
- 问题1解决方案：遇到版本问题，阅读最新版的官方文档，可以查到每个版本的update记录，并且可以查到相应的解决办法。有些情况下，Android Studio也会提示修改方法。                    


# 代码调试中的问题和解决过程
- 问题1：在Constraint布局中拖入一个button（或其他控件）报错如下：    
![](https://img2018.cnblogs.com/blog/1511102/201904/1511102-20190406142336902-1324236817.png)
   
- 问题1解决方案：ConstraintLayout属于Android Studio 2.2的新特性<del>意味着书上没有</del>，ConstraintLayout是使用约束的方式来指定各个控件的位置和关系的，有点类似于RelativeLayout，但远比RelativeLayout要更强大。    
这个报错的意思就是，这个view没有被约束，它仅仅有设计时的位置，因此它将跳到（0，0），除非你添加约束。     
那么，如何添加约束呢？有两种方式：一种是Android Studio自带的自动添加约束的一个工具，即下图中的红框中的“Infer Constraints”。   
![](https://img2018.cnblogs.com/blog/1511102/201904/1511102-20190406142405899-1067554077.png)


另一种是自己手动设置约束，方法也很简单，即将控件四周的点引向想要让控件被约束的边，具体方法见[Android新特性介绍，ConstraintLayout完全解析](https://blog.csdn.net/guolin_blog/article/details/53122387)     
![](https://img2018.cnblogs.com/blog/1511102/201904/1511102-20190406142421208-869970355.png)

# [代码托管](https://gitee.com/PNIDEMOOO/zhy20189217)
![](https://img2018.cnblogs.com/blog/1511102/201904/1511102-20190406152235581-683056232.png)



# 上周考试错题总结
- 错题1:    
	在使用高级并行API时，（）接口的操作对象可以实现Object的wait()、notify()、notifyAll()功能？     
	A.Lock  
	B.Condition   
	C.Future   
	D.Callable    
	正确答案：B.Condition    

- 错题2：   
调用线程的interrupt()方法 ，会抛出（）异常对象？  
	A.IOException     
	B.IllegalStateException   
	C.RuntimeException   
	D.InterruptedException   
	E.SecurityException   
	正确答案：DE     

# 学习进度条

|            | 代码行数（新增/累积）| 博客量（新增/累积）|学习时间（新增/累积）|重要成长|
| --------   | :----------------:|:----------------:|:---------------:  |:-----:|
| 目标        | 5000行            |   30篇           | 400小时            |       |
| 第一周      | 200/200           |   2/2            | 20/20             |       |
| 第二周      | 300/500           |   1/3            | 17/37             |       |
| 第三周      | 787/1287          |   1/4            | 15/52             |       |
| 第四周      | 350/1637          |   1/5            | 16/68             |       |    
| 第五周      | 1049/2686         |   1/6            | 20/88             |       |   
| 第六周      | 1500/4186         |   1/7            | 18/106            |       |


- 计划学习时间:18小时

- 实际学习时间:20小时


# 参考资料

-  [Java和Android开发学习指南（第二版）](https://book.douban.com/subject/26986744/)
-  [Android新特性介绍，ConstraintLayout完全解析](https://blog.csdn.net/guolin_blog/article/details/53122387)   