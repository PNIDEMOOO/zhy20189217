# 教材学习内容总结
## 第43章要点
### 要点1：录制视频   
录制视频需要用到麦克风和摄像头硬件，还需要赋予一些相应的权限。

     <uses-permission android:name="android.permission.RECORD_AUDIO"/>
     <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"/>
     <uses-permission android:name="android.permission.CAMERA"/>
     <uses-permission android:name="android.permission.MOUNT_UNMOUNT_FILESYSTEMS"/> 

## 第44章要点
### 要点1：MediaRecorder类  
* MediaRecorder 是 MediaStream Recording API 提供的用来进行媒体轻松录制的接口, 他需要通过调用 MediaRecorder() 构造方法进行实例化。  
* MediaRecorder录像录音必须按照API说明的调用顺序依次调用，否则报错。可能会出现无法调用start()方法或者调用start()后闪退。  
* MediaRecorder的输出可以写入到一个文件。   
* MediaRecorder依赖硬件，不同手机上可能需要不同的配置。  

## 第45章要点
### 要点1：Handler
#### Handler类的引入 
![](https://img2018.cnblogs.com/blog/1511102/201905/1511102-20190512010051379-1674289203.jpg)



#### Handler的执行流程图   
![](https://img2018.cnblogs.com/blog/1511102/201905/1511102-20190512010100913-1201423193.jpg)


  

* UI线程:就是我们的主线程,系统在创建UI线程的时候会初始化一个Looper对象,同时也会创建一个与其关联的MessageQueue；   
* Handler:作用就是发送与处理信息,如果希望Handler正常工作,在当前线程中要有一个Looper对象；     
* Message:Handler接收与处理的消息对象；     
* MessageQueue:消息队列,先进先出管理Message,在初始化Looper对象时会创建一个与之关联的MessageQueue； 
* Looper:每个线程只能够有一个Looper,管理MessageQueue,不断地从中取出Message分发给对应的Handler处理。  


## 第46章要点
### 要点1：AsyncTask类
* android.os.AsyncTask类是一个工具类，它使得处理后台进程以及将进度更新发布到UI线程更加容易。   
* 这个类专门用于持续**最多数秒钟**的较短的操作，对于长时间运行的后台任务，应该使用Java并发工具框架。  
* AsyncTask类带有一组公有的方法和一组受保护的方法。公有方法用于执行和取消其任务。execute方法启动一个异步的操作，而cancel方法取消该操作。受保护的方法是供你在子类中覆盖的。doInBackground方法就是一个受保护的方法，它是该类中最重要的方法，并且为异步操作提供了逻辑。   
* 还有一个publishProgress方法，也是受保护的方法，它通常从doInBackground中调用多次，通常在该方法中编写代码更新一个进度条或其他UI组件。  


# 代码调试中的问题和解决过程
- 问题1：

		<TextView
        android:id="@+id/textView1"
        android:layout_width="wrap_content"
        android:layout_height="50dp"
        android:layout_weight="1"
        android:background="#990033"
        android:text="111111111" />

		<TextView
        android:id="@+id/textView2"
        android:layout_width="wrap_content"
        android:layout_height="50dp"
        android:layout_weight="3"
        android:background="#990033"
        android:text="222222222" />

    二者的宽度并没有严格按照1:3的比例分配。     

     
- 问题1解决方案：    
因为layout_width属性并不对整个可用空间进行分配，而是对剩余空间进行分配，也就是说系统会先按layout_width设置的属性先给控件分配空间，在这里的代码里是先分配空间使得每个控件足以包裹住内容，再将剩余的内容按layout_weight所设置的比例进行分配，控件最终的空间大小是layout_width属性设置的空间大小加上按比例分得的剩余空间大小，所以除去内容之外的控件还是1:3。  
解决方法是：将二者的width都设为0dp，即<code_s>android:layout_width="0dp"</code_s>。   

# [代码托管](https://gitee.com/PNIDEMOOO/zhy20189217)
![](https://img2018.cnblogs.com/blog/1511102/201905/1511102-20190512223523320-1068946873.png)



# 学习进度条

|            | 代码行数（新增/累积）| 博客量（新增/累积）|学习时间（新增/累积）|重要成长|
| --------   | :----------------:|:----------------:|:---------------:  |:-----:|
| 目标        | 5000行            |   30篇           | 400小时           |       |
| 第一周      | 200/200           |   2/2            | 20/20             |       |
| 第二周      | 300/500           |   1/3            | 17/37             |       |
| 第三周      | 787/1287          |   1/4            | 15/52             |       |
| 第四周      | 350/1637          |   1/5            | 16/68             |       |    
| 第五周      | 1049/2686         |   1/6            | 20/88             |       |   
| 第六周      | 1500/4186         |   1/7            | 18/106            |       |
| 第七周      | 1718/5904         |   1/8            | 14/120            |       |
| 第八周      | 200/6104          |   1/9            | 12/132            |       |
| 第九周      | 800/6904          |   2/11           | 12/144            |       |
| 第十周      | 800/7704          |   1/12           | 15/159            |       |
| 第十一周    | 500/8204          |   1/13           | 20/179            |       |


- 计划学习时间:18小时

- 实际学习时间:20小时


# 参考资料
-  [Java和Android开发学习指南（第二版）](https://book.douban.com/subject/26986744/)         
-  [3.3 Handler消息传递机制浅析](https://www.runoob.com/w3cnote/android-tutorial-handler-message.html)