# 教材学习内容总结
## 第12章要点：
### 要点1：enum
* enum类可以独立使用，也可以作为类的一部分。如果需要在应用程序中的多个地方引用它的话，就让它单独使用。如果只是用于一个类中，最好让enum成为这个类的一部分。   
* enum值是区分大小写的，且按惯例要大写。   
* 在保证变量只能被赋给一个有效值这方面，enum比静态final更好。   

### 要点2：java.lang.Enum类 
* enum类没有公有的构造函数，这使得不可能将其实例化。   
* 它们隐式地是静态的。   
* 每个enum常量只有一个实例。   
* 可以在enum上调用values方法，以遍历其枚举值，该方法返回对象的一个数组。   

### 要点3：枚举成员  
* 一个enum类可以有构造方法和方法，如果有构造方法，其访问级必须是私有的或默认的，如果一个enum定义包含了常量以外的其他内容，常量必须在其它内容之前定义，且最后的常量用一个分号结束。     

## 第13章要点：
### 要点1：  
|类|表示|适用于|
|--|--|--|
|Instant|表示时间线上的一个**时间点**|计时|
|LocalDate|没有时间部分和时区部分的一个**日期**|生日|
|LocalDateTime|**日期和时间**|订单配送日期时间|
|LocalTime|只有**时间**没有日期|不关心日期，只需要一个时间|
|ZonedDateTime|有**时区**|机场航班时间|
|Duration|**纳秒级**精度的**时间量**|建模航班时间|
|Period|**天数、月份**|计算年龄|
|DateTimeFormatter|格式化日期和时间|-|

## 第14章要点：
### 要点1：集合概述  
* 集合也叫容器，提供了一种方法来存储、访问和操作其元素。    
* Collection有3个主要的子接口：List、Set和Queue。    
* Collection接口将对象组织到一起。数组不能调整大小，并且只能组织相同类型的对象，而集合允许添加任何类型的对象，并且不强迫你指定初始大小。   
* Map接口可以用于存储键/值对。   

### 要点2：List和ArrayList  
* List又叫序列，是一个**有序**的集合。   
* ArrayList是最为常用的List的实现。    
* 可以使用索引来访问其元素，也可以在确切的位置插入一个元素。   
* List使用add来插入一个元素，用set来替换一个元素，用remove来删除一个元素。   
* List允许存储重复的元素。   
* Arrays类提供了一个asList方法，允许你一次向一个List添加数组或任意多个元素。    
* List的sort方法可以很容易地对一个List进行排序。   

### 要点3：遍历一个集合  
#### 方法1
	for (Iterator iterator = myList.iterator();iterator.hasNext()){
		...
	}

#### 方法2
	for (Object object:myList){
		...
	}

>方法2是方法1的快捷方式。   

### 要点4：Set和HashSet
* 和List不同，Set不允许重复的内容。
* Set允许有最多一个空元素。   
* Set最流行的实现是HashSet。   

### 要点5：集合转换  
将一个Queue转换为一个List：

	Queue queue = new LinkedList();
	queue.add("hello");
	List list = new ArrayList();

将一个List转换为一个Set：

	List myList = new ArrayList();
	myList.add("hello");
	Set set = new HashSet(myList);

### 要点6：Map和HashMap
* Map保存了键到值的映射。   
* Map中不能有重复的元素，并且每个键最多映射一个值。   
* Map有几个实现，HashMap是非同步的，Hashtable是同步的。   

## 第15章要点：
### 要点1：泛型类型  
* 反省类型常被叫做参数性类型，可以接受参数。   
* 声明一个泛型类型：   
	<code_l>MyType < typeVar1, typeVar2, ... > </code_l>   
	e.g. 

		List<E> myList; 

	E就是所谓的类型变量，也就是能用一个类型替代的变量。  
* 要实例化一个泛型类型，在声明的时候，传入相同的参数列表。例如，要创建使用String的一个ArrayList，在尖括号中传入String。   

		List<String> myList = new ArrayList<String>();   

	在Java7及之后的版本中，还可以写得更精简：   

		List<String> myList = new ArrayList<>();    

* 使用泛化类型，类型检查在编译时进行。   

### 要点2：使用?通配符  
* List<?>表示任意类型的对象的一个列表。   
* 在声明或创建一个泛型类型的时候，使用通配符是非法的，如下所示：   
	
		List<?> myList = new ArrayList<?>();   //非法    

* 想要创建接受任意类型的对象的一个List，使用Object作为类型变量，如下面代码所示：   

		List<Object> myList = new ArrayList<>();  


# 教材学习中的问题和解决过程
- 问题1：为什么有些enum类要有构造函数？    
- 问题1解决方案：通过看一些别人写的代码发现，这类enum的构造函数是为了添加枚举元素，设置枚举元素上下界等功能而存在的。   


# 代码调试中的问题和解决过程
- 问题1：搭档的代码下载下来之后IDEA打开无法build。        
- 问题1解决方案：需要在project structure中手动设置test、sources等目录的路径。另外，需要统一language level。   
![](https://img2018.cnblogs.com/blog/1511102/201903/1511102-20190324152805658-1688777921.png)

# [代码托管](https://gitee.com/PNIDEMOOO/zhy20189217)
![](https://img2018.cnblogs.com/blog/1511102/201903/1511102-20190324155222960-2104606878.png)



# 上周考试错题总结
- 错题1   

	<code_l>double d = 0.1 + 0.1 + 0.1, System.out.println(i);的结果是0.3.【错误】  </code_l>  

	因为double是个近似值，它四舍五入后还是0.3。0.3化成二进制其实是个无穷小数，保存成double，只能是个近似值

- 错题2  

	<code_l>Java中try…catch…final用来处理异常。【错误】</code_l>    

	Java中用try…catch…finally来处理异常。   


# 学习进度条

|            | 代码行数（新增/累积）| 博客量（新增/累积）|学习时间（新增/累积）|重要成长|
| --------   | :----------------:|:----------------:|:---------------:  |:-----:|
| 目标        | 5000行            |   30篇           | 400小时            |       |
| 第一周      | 200/200           |   2/2            | 20/20             |       |
| 第二周      | 300/500           |   1/3            | 17/37             |       |
| 第三周      | 787/1287          |   1/4            | 15/52             |       |
| 第四周      | 350/1637          |   1/5            | 16/68             |       |                


- 计划学习时间:18小时

- 实际学习时间:16小时


# 参考资料

-  [Java和Android开发学习指南（第二版）](https://book.douban.com/subject/26986744/)