# 教材学习内容总结
## 第27章要点：
### 要点1：Android布局  
如下是Android中的一些布局。  

* LinearLayout。将所有子视图以相同的方向（或者水平地或者垂直地）对齐的一个布局。   
* RelativeLayout。根据姿势同的一个或多个同级视图的位置来排列它的一个布局。    
* FrameLayout。将每一个子视图放在另一个子视图顶部的一种布局。   
* TableLayout。将子视图按照行和列来组织的一种布局。   
* GridLayout。将子视图放置到一个栅格中的一种布局。 
* <code_s>ConstraintLayout</code_s>。按照约束布局组件。ConstraintLayout是Android Studio 2.2中主要的新增功能之一。     

> 大多数情况下，布局中的一个视图必须拥有layout_width和layout_height属性，以便布局知道如何调整视图的大小。layout_width和layout_height属性可以赋值为match_parent（和父视图的宽度和高度一致）、wrap_content（与其内容和高度一致），或者是一个度量单位。   

> AbsoluteLayout提供了子视图的精准定位，它现在已经废弃了，并且不应该再使用它。现在使用RelativeLayout来取代它。   


### 要点2：居中
### LinearLayout布局如何居中
* <code_s>android:layout_gravity="center_horizontal"</code_s>表示**该视图在父布局里**水平居中，此时其父布局必须拥有<code_s>android:orientation="vertical"</code_s>属性；

* <code_s>android:layout_gravity="center_vertical"</code_s>表示**该视图在父布局里**垂直居中，此时其父布局必须应设置成<code_s>android:orientation="horizontal"</code_s>属性（默认为该属性），且其父布局的高度应设置为<code_s>android:layout_height="fill_parent"</code_s>属性；   

>注意区分<code_s>android:gravity="center_horizontal"</code_s>和<code_s>android:layout_gravity="center_horizontal"</code_s>  
前者是指让该视图里的内容水平居中，后者是指让该视图在其父视图里水平居中。   

### RelativeLayout布局如何居中
* android:layout_centerHorizontal="true" 该视图在父视图中水平居中;
* android:layout_centerVertical="true" 该视图在父视图中垂直居中;
* android:layout_centerInParent="true" 该视图在父视图中垂直和水平居中;   

## 第28章要点：
### 要点1：基于监听的事件处理机制
事件监听机制中由事件源，事件，事件监听器三类对象组成。   
处理流程如下: 

* 1、为某个事件源(组件)设置一个监听器,用于监听用户操作   
* 2、用户的操作,触发了事件源的监听器   
* 3、生成了对应的事件对象   
* 4、将这个事件源对象作为参数传给事件监听器   
* 5、事件监听器对事件对象进行判断,执行对应的事件处理器(对应事件的处理方法)     

### 要点2：设置点击监听的5种方式
#### 方法1：直接用匿名内部类
>这是最常用的一种方法，直接setXXXListener后,重写里面的方法即可。但通常是临时使用一次,复用性不高。   

	public class MainActivity extends Activity {    
	    private Button btnshow;    
	        
	    @Override    
	    protected void onCreate(Bundle savedInstanceState) {    
	        super.onCreate(savedInstanceState);    
	        setContentView(R.layout.activity_main);    
	        btnshow = (Button) findViewById(R.id.btnshow);    
	        btnshow.setOnClickListener(new OnClickListener() {    
	            //重写点击事件的处理方法onClick()    
	            @Override    
	            public void onClick(View v) {    
	                //显示Toast信息    
	                Toast.makeText(getApplicationContext(), "你点击了按钮", Toast.LENGTH_SHORT).show();    
	            }    
	        });    
	    }        
	} 

#### 方法2：使用内部类   
>可以在该类中进行复用,可直接访问外部类的所有界面组件。

	public class MainActivity extends Activity {    
	    private Button btnshow;    
	    @Override    
	    protected void onCreate(Bundle savedInstanceState) {    
	        super.onCreate(savedInstanceState);    
	        setContentView(R.layout.activity_main);    
	        btnshow = (Button) findViewById(R.id.btnshow);    
	        //直接new一个内部类对象作为参数    
	        btnshow.setOnClickListener(new BtnClickListener());    
	    }     
	    //定义一个内部类,实现View.OnClickListener接口,并重写onClick()方法    
	    class BtnClickListener implements View.OnClickListener    
	    {    
	        @Override    
	        public void onClick(View v) {    
	            Toast.makeText(getApplicationContext(), "按钮被点击了", Toast.LENGTH_SHORT).show();   
	        }    
	    }    
	} 


#### 方法3：使用外部类  
>另外创建一个处理事件的Java文件,这种形式用的比较少，因为**外部类不能直接访问用户界面类中的组件**,要通过构造方法将组件传入使用。

**MyClick.java**：

	public class MyClick implements OnClickListener {    
	    private TextView textshow;    
	    //把文本框作为参数传入    
	    public MyClick(TextView txt)    
	    {    
	        textshow = txt;    
	    }    
	    @Override    
	    public void onClick(View v) {    
	        //点击后设置文本框显示的文字    
	        textshow.setText("点击了按钮!");    
	    }    
	}


**MainActivity.java**：

	public class MainActivity extends Activity {    
	    private Button btnshow;    
	    private TextView txtshow;    
	    @Override    
	    protected void onCreate(Bundle savedInstanceState) {    
	        super.onCreate(savedInstanceState);    
	        setContentView(R.layout.activity_main);    
	        btnshow = (Button) findViewById(R.id.btnshow);    
	        txtshow = (TextView) findViewById(R.id.textshow);    
	        //直接new一个外部类，并把TextView作为参数传入    
	        btnshow.setOnClickListener(new MyClick(txtshow));    
	    }         
	} 

#### 方法4：直接使用Activity作为事件监听器
>让Activity类实现XxxListener事件监听接口,在Activity中定义重写对应的事件处理器方法，这种方法用的比较少。

	//让Activity方法实现OnClickListener接口    
	public class MainActivity extends Activity implements OnClickListener{    
	    private Button btnshow;    
	    @Override    
	    protected void onCreate(Bundle savedInstanceState) {    
	        super.onCreate(savedInstanceState);    
	        setContentView(R.layout.activity_main);    
	            
	        btnshow = (Button) findViewById(R.id.btnshow);    
	        //直接写个this    
	        btnshow.setOnClickListener(this);    
	    }    
	    //重写接口中的抽象方法    
	    @Override    
	    public void onClick(View v) {    
	        Toast.makeText(getApplicationContext(), "点击了按钮", Toast.LENGTH_SHORT).show();         
	    }         
	}  

#### 方法5：直接绑定到标签  
>在xml布局文件中对应得Activity中定义一个事件处理方法。

**MainAcivity.java**:

	public class MainActivity extends Activity {    
	    @Override    
	    protected void onCreate(Bundle savedInstanceState) {    
	        super.onCreate(savedInstanceState);    
	        setContentView(R.layout.activity_main);     
	    }    
	    //自定义一个方法,传入一个view组件作为参数    
	    public void myclick(View source)    
	    {    
	        Toast.makeText(getApplicationContext(), "按钮被点击了", Toast.LENGTH_SHORT).show();    
	    }    
	} 

**main.xml布局文件**:

	<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"    
	    xmlns:tools="http://schemas.android.com/tools"    
	    android:id="@+id/LinearLayout1"    
	    android:layout_width="match_parent"    
	    android:layout_height="match_parent"    
	    android:orientation="vertical" >    
	    <Button     
	        android:layout_width="wrap_content"    
	        android:layout_height="wrap_content"    
	        android:text="按钮"    
	        android:onClick="myclick"/>    
	 </LinearLayout> 

## 第29章要点：
### 要点1：返回键
* 返回键导航的显示与隐藏：setDisplayHomeAsUpEnabled(Boolean flag)
* 返回键导航的图标设置：setHomeAsUpIndicator
* 返回键的事件处理：重写onOptionsItemSelected方法

## 第30章要点：
### 要点1：Android菜单类型  
* 选项菜单  
* 上下文菜单  
* 弹出式菜单   

### 要点2：实现3种菜单的方法
#### 选项菜单/上下文菜单   
1、在一个XML文件中创建菜单，并将其保存到res/menu目录下。  
2、在活动类中，根据菜单类型，覆盖onCreateOptionsMenu或onCreateContextMenu方法。然后，在覆盖的方法中，调用getMenuInflater().inflate()传入要使用的菜单。   
3、在活动类中，根据菜单类型，覆盖onOptionsItemSelected或onContextItemSelected方法。   

#### 弹出式菜单
1、在一个XML文件中创建菜单，并将其保存到res/menu目录下。  
2、在活动类中，创建一个PopupMenu对象和一个PopupMenu.OnMenuItemClickListener对象。在监听器类中，定义一个方法，当选择一个弹出式菜单选项的时候，该方法将处理点击事件。   

# 教材学习中的问题和解决过程
- 问题1：在Android中，End和Right，Start和Left的区别。            
- 问题1解决方案：left是绝对的左边，start会根据不同的国家习惯改变。比如在从右向左顺序阅读的国家，start代表的就是在右边。                                

# 代码调试中的问题和解决过程
- 问题1：LinearLayout中控件无法居中。  

- 问题1解决方案：总结了相关资料，见27章要点2。    


# [代码托管](https://gitee.com/PNIDEMOOO/zhy20189217)



# 上周考试错题总结
上周无测试。        

# 学习进度条

|            | 代码行数（新增/累积）| 博客量（新增/累积）|学习时间（新增/累积）|重要成长|
| --------   | :----------------:|:----------------:|:---------------:  |:-----:|
| 目标        | 5000行            |   30篇           | 400小时            |       |
| 第一周      | 200/200           |   2/2            | 20/20             |       |
| 第二周      | 300/500           |   1/3            | 17/37             |       |
| 第三周      | 787/1287          |   1/4            | 15/52             |       |
| 第四周      | 350/1637          |   1/5            | 16/68             |       |    
| 第五周      | 1049/2686         |   1/6            | 20/88             |       |   
| 第六周      | 1500/4186         |   1/7            | 18/106            |       |
| 第七周      | 1718/5904         |   1/8            | 14/120            |       |


- 计划学习时间:18小时

- 实际学习时间:14小时


# 参考资料

-  [Java和Android开发学习指南（第二版）](https://book.douban.com/subject/26986744/)
-  [Android新特性介绍，ConstraintLayout完全解析](https://blog.csdn.net/guolin_blog/article/details/53122387)   
-  [Android布局中如何使控件居中](https://www.cnblogs.com/liangwenbo/p/4329034.html)   
-  [基于监听的事件处理机制](http://www.runoob.com/w3cnote/android-tutorial-listen-event-handle.html)   