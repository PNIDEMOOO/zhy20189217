# 教材学习内容总结
## 第31章要点
### 要点1：ListView
* ListView是一个可以显示滚动的列表项的一个视图，列表项可能来自于一个列表适配器或一个数组适配器。   
* 选取ListView中的一项，将会触发一个事件。   
* 如果一个活动只包含一个ListView视图，可以扩展ListActivity而不是Activity作为活动类。    
* ListView必须获取一个ListAdapter作为数据源。   
* ListAdapter为ListView中的每一项提供了布局。  

### 要点2：ListAdapter
* ListAdapter至少有两个实现可以使用：ArrayAdapter和SimpleAdapter。  
* ListAdapter类提供了几个构造方法，所有构造方法都需要传入一个Context以及一个资源标识符，后者指向一个包含TextView的布局。这是因为ListView中的每一项都是一个TextView。   


## 第32章要点
### 要点1：GridView
* GridView是包含了显示在表格中可滚动项的一个列表的视图。
* 与ListView一样，GridView也是通过一个ListAdapter获取数据和布局。   
* GridView还能接受一个AdapterView.OnItemClick Listener 以处理项的选取。   

##### GridView主要方法
* ListAdapter:getAdapter()   
返回关联的Adapter  
* int:getColumnWidth()   
返回列的宽度  
* int:getGravity()   
返回描述子视图被放置的方式的标识。默认为Gravity.LEFT。   
* int:getHorizontalSpacing()   
返回列间的水平间隔大小。    
* int:getNumColumns()    
返回列数。如果网格没有被布局，则返回AUTO_FIT。   
* int:getRequestedColumnWidth()   
返回请求的列宽度。    
* int:getRequestedHorizontalSpacing()    
返回请求的列间的水平间隔。     
* int:getStretchMode()   
返回扩展模式。  
* int:getVerticalSpacing()   
返回行间的垂直间隔。   
* onInitializeAccessibilityNodeInfoForItem(View view, int position, AccessibilityNodeInfo info)    
使用列表中实际项的信息初始化一个AccessibilityNodeInfo。  


## 第33章要点
### 要点1：样式 
* 创建一个样式的优点在于，能够让样式变得**可复用**和**可共享**。   
* style属性没有android前缀。  

### 要点2：主题
* 主题是应用于一个活动或者整个应用程序的一个样式。   
* 要对一个活动应用一个主题，可以使用清单文件中的activity元素中的android:theme属性。  
例如：  

		<activity
			android:name="..."   
			android:theme="@android:style/Theme.Holo.Light">
		</activity>



## 第34章要点
### 要点1：位图 
* 位图（bitmap）是一种图像文件格式，它可以独立于显示设备来存储数字图像。   
* 位图包括支持有损压缩和无损压缩的其他格式，包括JPEG、GIF和PNG格式。GIF和PNG格式支持透明度和无损压缩，而JPEG格式支持有损压缩，并且不支持透明度。   
* 表示数字图像的另一种方式使使用数学表达式。这样的图像叫作矢量图。   
* Bitmap类对一个位图建模。Bitmap可以显示在一个使用了ImageView微件的活动中。   
* 加载位图最容易的方法就是使用BitmapFactory类，它提供了静态的方法，可以从一个文件、一个字节数组、一个Android资源或一个InputStream类来构建一个Bitmap。   

# 教材学习中的问题和解决过程
- 问题1：   
位图与矢量图有什么区别？             
- 问题1解决方案：         

||位图|矢量图|  
|--|--|--|  
|分辨率|由一个一个像素点产生，当放大图像时，像素点也放大了，但每个像素点表示的颜色是单一的，所以在位图放大后就会出现马赛克状。|与分辨率无关，可以将它缩放到任意大小和以任意分辨率在输出设备上打印出来，都不会影响清晰度。 |   
|色彩丰富度|表现的色彩比较丰富，可以表现出色彩丰富的图象，可逼真表现自然界各类实物。|色彩不丰富，无法表现逼真的实物，矢量图常常用来表示标识、图标、Logo等简单直接的图像。 |    
|文件类型|.bmp、.pcx、.gif、.jpg、.tif、.png、photoshop的 .psd等。|AdobeIllustrator的 .AI、.EPS和SVG、AutoCAD的 .dwg和dxf、Corel DRAW的 *.cdr等。|  
|占用空间|表现的色彩比较丰富，所以占用的空间会很大，颜色信息越多，占用空间越大，图像越清晰，占用空间越大。|表现的图像颜色比较单一，所以所占用的空间会很小。|    


# [代码托管](https://gitee.com/PNIDEMOOO/zhy20189217)
![](https://img2018.cnblogs.com/blog/1511102/201904/1511102-20190422082812729-455611064.png)


# 上周考试错题总结
-  错题1：    
	写程序时，应当写三种代码：  
	A.Java代码  
	B.测试代码  
	C.产品代码  
	D.伪代码   
	正确答案：BCD


# 学习进度条

|            | 代码行数（新增/累积）| 博客量（新增/累积）|学习时间（新增/累积）|重要成长|
| --------   | :----------------:|:----------------:|:---------------:  |:-----:|
| 目标        | 5000行            |   30篇           | 400小时            |       |
| 第一周      | 200/200           |   2/2            | 20/20             |       |
| 第二周      | 300/500           |   1/3            | 17/37             |       |
| 第三周      | 787/1287          |   1/4            | 15/52             |       |
| 第四周      | 350/1637          |   1/5            | 16/68             |       |    
| 第五周      | 1049/2686         |   1/6            | 20/88             |       |   
| 第六周      | 1500/4186         |   1/7            | 18/106            |       |
| 第七周      | 1718/5904         |   1/8            | 14/120            |       |
| 第八周      | 200/6104           |   1/9            | 12/132            |       |

- 计划学习时间:18小时

- 实际学习时间:12小时


# 参考资料

-  [Java和Android开发学习指南（第二版）](https://book.douban.com/subject/26986744/)   
-  [GridView使用详解](https://blog.csdn.net/lyy666888/article/details/79163632)   
-  [位图（标量图）与矢量图区别](https://blog.csdn.net/wxl1555/article/details/80651016)    