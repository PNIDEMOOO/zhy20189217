# 教材学习内容总结
## 第四章要点：
### 要点1：Java类命名规范
* **类名**采用Pascal命名惯例，即类名中每个单词首字母大写，e.g. PostOffice   
* **方法名**和**字段名**采用骆驼命名惯例，即除了第一个单词外，将每个单词首字母大写，e.g. regulatRateCalculator  

### 要点2：Main方法
main是一种特殊的方法，它提供了应用程序的**入口点**。应用程序通常有很多的类，但只有**一个**类需要有main方法。该方法允许包含的类调用它。  
main方法的签名如下：   

	public static void main (String[] args) 

当使用java运行一个类的时候，可以给main传递参数，语法如下：   
<code_l>java className arg1 arg2 arg3 ...  </code_l>
### 要点3：构造函数
每个类至少有一个构造函数，否则无法从类创建出对象，且类也毫无用处。  
构造方法不需要有返回值（void也不需要）。   

>特别注意：构造方法的名称必须与类名相同！

### 要点4：Varargs
Varargs是一项Java功能，允许方法拥有一个可变长度的参数列表。   
例如：   

	public double average (int... args)

省略号表示有0个或多个int型参数。   

### 要点5：创建对象  
创建一个对象：  

	Employee employee = new Employee();

这里的employee是Employee类型的一个对象引用。   

### 要点6：对象的内存分配  
注意对比以下两个例子：  
例1：    

	Book mybook = new Book();
	myBook.numberOfPages = 220;
	Book yourBook = new Book();
	yourBook.numberOfPages = 239;

这四行代码的结果是yourBook.money变成了239，myBook.money变成了220，因为myBook和yourBook分别是两个不同对象的引用，内存中为二者分别分配了一个Book对象空间。   

例:2：   

	Book mybook = new Book();
	myBook.numberOfPages = 220;
	Book yourBook = myBook;
	yourBook.numberOfPages = 239;

这四行代码的结果是yourBook.money和myBook.money都变成了239，因为youBook和myBook是同一个对象的多个引用，内存中只分配了一个Book对象空间。   
### 要点7：封装和访问控制
#### 类访问控制修饰符
	
	package app04;
	public class Book{
		...
	}

这里的Book类是包app04的一个成员，访问控制修饰符<code_s>public</code_s>表示该类是公有的，任何类均可以实例化它。  

> 注：一个Java源文件只能包含一个public类，但它可以包含多个不是公有的类。  

当一个类声明的前面没有访问控制修饰符的时候，这个类是默认的访问级别。例如：  

	package app04;
	class Chapter {
		...
	}

这里的Chapter就是具有默认访问级别的类，它只能由属于同一个包的类使用。例如，Chapter能在Book中实例化，但对其他包中的类是不可见的。   

#### 类成员访问控制修饰符
|访问级别|从其他包中的类访问|从同一包中的类|从子类|从同一个类|
|--|--|--|--|--|
|public|可以|可以|可以|可以|
|protected|不可以|可以|可以|可以|
|default|不可以|可以|不可以|可以|
|private|不可以|不可以|不可以|可以|

### 要点8：this关键字
在引用当前对象的任何方法或构造方法的时候使用this关键字。例如，一个类级别的字段和一个局部变量具有相同的名字，可以使用<code_s>this.field</code_s>语法来引用前者。下面是一个具体的例子：      

	package app04;
	public class Box {
	    int length;
	    int width;
	    int height;
	    public Box(int length, int width, int height) {
	        this.length = length;
	        this.width = width;
	        this.height = height;
	    }
	}

### 要点9：使用其他的类
关键字<code_s>import</code_s>用于引入一个包或者一个包中的类。   

	package app04;
	import java.util.ArrayList;
	public class Demo {
		...
	}

> 注：import必须放在package语句之后，类声明之前。

### 要点10：静态成员  
Java支持静态成员的表示法。静态成员是类成员，不用先实例化该类就可以调用它们。例如，java.lang.System中的out字段就是静态的；充当一个类的入口点的main方法也是静态的，因为它必须在创建任何对象之前调用。声明静态成员（字段或方法）之前加关键字<code_s>static</code_s>。   
在静态方法内部不能调用实例方法或实例字段，但可以访问其他的静态方法或静态字段。   

### 要点11：静态final变量 
静态final变量的命名惯例是，全部大写，且两个单词之间使用一个下划线隔开，如

	static final int NUMBER_OF_MONTH = 12;

另外，静态final引用变量也是可能的，但只有该变量是final的。一旦给它分配了一个实例的地址，就不能把相同类型的另一个变量赋值给它，但引用对象自身之中的字段是可以修改的。

## 第七章要点 
### 要点1：is-a关系
	class Animal {
		...
	}

	class Bird extends Animal {
		...
	}

这里Bird是超类Animal的一个子类，将子类的一个实例赋值给父类的一个引用变量是合法的，但将父类的一个实例复制给子类的一个引用变量是不合法的。   

	Animal animal = new Bird();//合法

	Bird bird = new Animal();//不合法

### 要点2：可访问性 
	package app07;
	public class P {
	    public void publicMethod() {
	    }
	    protected void protectedMethod() {
	    }
	    void defaultMethod() {
	    }
	}
	class C extends P {
	    public void testMethods() {
	        publicMethod();
	        protectedMethod();
	        defaultMethod();
	    }
	}

这里P有三个方法，一个public，一个protected，一个default。  

	package test;  
	import app07.C;
	public class AccessbilityTest{
		public static void main(String[] args){
		C c = new C();
		c.protectedMethod(); //无法编译
		}
	}

protectedMethod是P的一个受保护的方法。在P之外不能访问它，除非通过一个子类来访问它。但AccessbilityTest并不是P的子类，在其中，通过P的子类C也无法访问P的受保护方法。

### 要点3：方法覆盖 
* 子类可以覆盖父类公开的和受保护的方法，如果二者在同一包中，还可以覆盖带有默认访问级别的方法。   
* 子类将超类中的一个方法的可见性从受保护的曾嘉伟公有的，这种做法是允许的。然而，减少可见性是非法的。   
* 被覆盖的方法通常使用<code_s>@Override</code_s>来标记。
* 不能覆盖一个final方法。   

### 要点4：调用超类的构造方法
* 调用子类的一个构造方法来实例化一个子类时，该构造方法会首先调用直接父类的**无参数**的构造方法。   
* 使用super关键字，从一个子类的构造方法中显示调用父类的构造方法，但是super必须是构造方法中的第一条语句。   
* 可以使用super(null)调用父类中不存在的构造方法。   

### 要点5：调用超类的隐藏方法 
* super还有一个作用是可以用它来调用超类中的隐藏的成员或隐藏的方法。  

### 要点6：类型强制转化
向上转换：将子类的一个实例强制转型为其父类。但父类引用变量不能访问那些只能在子类中可用的成员。   
向下转换：只有当父类的引用已经指向了子类的一个实例的时候，才允许将一个子类向下强制转型。   

### 要点7：final类
在类声明中使用关键字<code_s>final</code_s>，可组织其他类扩展该类。   

## 第十章要点
### 要点1：接口
* 除了明确指定，所有的接口方法都是抽象的。实现类必须覆盖接口中的所有抽象方法。   
* 接口中的字段必须初始化，并且隐式地是**公有的、静态的和final**（不需要把这三个关键字显式地写出来）的。   
* 按照惯例，接口中的字段名全部大写。   
* 接口中的抽象方法没有主体，由一个分号立即结束。  
* 所有的抽象方法，隐式地都是公有的和抽象的（不用显式地把<code_s>public</code_s>和<code_s>abstract</code_s>写出来）。   
* 一个接口可以扩展另一个接口。    

### 要点2：抽象类 
* 在类声明中使用关键字<code_s>abstract</code_s>来创建一个抽象类。   
* 抽象类扮演着类似接口的角色，同时还提供了部分实现。   

## 第十一章要点
### 要点1：多态
* 多态是一种OOP特性，它允许一个对象根据接收到的一个方法调用来确定要调用哪一个方法实现。   
* 多态对于静态方法无效，因为它们是前期绑定的。   
* 多态对于final方法也无效，以为无法扩展final方法。    

# 教材学习中的问题和解决过程
- 问题1：    
在整本书中没有看到关于指针的章节，java中到底有没有指针？

- 问题1解决方案：   
查找了相关的资料后，我得出的结论是：Java中并没有显示的使用指针，而且也不允许编程的过程中使用指针。   
实际上，java力图使程序员忘记指针<del>【 不，我忘不掉_(:з」∠)_ 】</del>，java去除了指针运算，并从语法上努力隐藏指针，让指向某对象的指针看起来更像那个对象本身。   

- 问题2：java中的引用和C/C++中的引用是一回事吗？
- 问题2解决方案：  
C/C++的引用，它跟java的引用完全不是一个东西，C/C++的引用是同一块内存的不同名字。而java的引用是指向一个对象，引用本身也占用了内存。   
java的引用可以看成是一个功能受限的指针。

# 代码调试中的问题和解决过程
- 问题1：在一个包的两个.java文件中，定义了两个名称相同的类，编译错误。   
- 问题1解决方案：修改其中一个类的名称。    
   
- 问题2：在一个.java文件中，定义了两个public类，编译错误。   
- 问题2解决方案：将其中一个类改为非公有即可。    
   
# [代码托管](https://gitee.com/PNIDEMOOO/zhy20189217)
![](https://img2018.cnblogs.com/blog/1511102/201903/1511102-20190310160543441-163660761.png)

# 上周考试错题总结
- 错题1   
	如果在hello.java中撰写以下的程序代码：   
	   
		public class Hello { 
		    public static void main(String[]　args) { 
		         System.out.println("Hello World"); 
		  }  
		}  

	以下描述正确的是（）？  
	  
	A .执行时显示Hello World  
	B .执行时出现NoClassDefFoundError  
	C .执行时出现出现找不到主要方法的错误  
	D .编译失败  

	**错误原因**：没有看到题目上的hello.java 和Hello的类名不一致，public类的文件名要和类名完全一致。

- 错题2
	如果有以下的程序片段：   

		System.out.println(-Integer.MAX_VALUE==Integer.MIN_VALUE);

	以下描述正确的是   
	A .执行时显示true   
	B .执行时显示false   
	C .执行时出现错误   
	D .编译失败    

	**错误原因**：此题存疑，Integer.MAX_VALUE 值为 2147483647，Integer.MIN_VALUE 值为  -2147483648，且此语句运行结果显示false。    

    
# 思考感悟
在本周的学习中，我发现Java和之前学过的另一个OOP语言C++还是有许多不同的地方的，比如Java中并没有指针这个概念，并且也不允许编程的过程中使用指针；C++中的“引用”和Java中的“引用”也有非常大的区别。我认为应该在学习过程中多多对不同语言之间类似的概念做对比，以便加深理解记忆。


# 学习进度条

|            | 代码行数（新增/累积）| 博客量（新增/累积）|学习时间（新增/累积）|重要成长|
| --------   | :----------------:|:----------------:|:---------------:  |:-----:|
| 目标        | 5000行            |   30篇           | 400小时            |       |
| 第一周      | 200/200           |   2/2            | 20/20             |       |
| 第二周      | 300/500           |   1/3            | 17/37             |       |


- 计划学习时间:20小时

- 实际学习时间:17小时


# 参考资料

-  [Java和Android开发学习指南（第二版）](https://book.douban.com/subject/26986744/)