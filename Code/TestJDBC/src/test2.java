import java.sql.*;
public class test2 {
    public static void main(String[] args) {
        Connection con;
        Statement sql;
        ResultSet rs;
        con = GetConnection.connectDB("world","root","");
        if(con == null) return;
        String sqlStr = "select * from country where Region = 'Middle East'";
        try {
            sql = con.createStatement();
            rs = sql.executeQuery(sqlStr);
            long totalpopulation = 0;
            while(rs.next()) {
                int Population = rs.getInt(7);
                totalpopulation +=Population;
            }
            System.out.println("中东国家的总人口为："+totalpopulation);
            con.close();
        }
        catch (SQLException e) {
            System.out.println(e);
        }
    }
}