import junit.framework.TestCase;
import org.junit.Test;

public class ComplexTest extends TestCase {
    Complex c1 = new Complex(2,3);
    Complex c2 = new Complex(3,4);
    Complex add = new Complex(5,7);
    Complex sub = new Complex(-1,-1);
    Complex mul =  new Complex(-6,17);
    Complex div = new Complex(0.72,0.04);
    String A = "LEE";
    int B = 0;
    @Test
    public void testNormal(){
        assertEquals(add.toString(),c1.ComplexAdd(c2).toString());
        assertEquals(sub.toString(),c1.ComplexSub(c2).toString());
        assertEquals(mul.toString(),c1.ComplexMulti(c2).toString());
        assertEquals(div.toString(),c1.ComplexDiv(c2).toString());
        //lee
        assertEquals(false,c2.equals(c1));
        assertEquals(false,c2.equals(A));
        assertEquals(false,c2.equals(B));



    }

    @Test
    public void testExceptions(){

    }

    @Test
    public void testBoundrys(){

        assertEquals(false,c2.equals(c1));
    }


}