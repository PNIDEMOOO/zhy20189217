public class Complex {
    // 定义属性并生成getter,setter
    double RealPart;
    double ImagePart;

    public double getRealPart() {
        return RealPart;
    }

    public double getImagePart() {
        return ImagePart;
    }

    // 定义构造函数
    public Complex() {
        RealPart = 0;
        ImagePart = 0;
    }

    public Complex(double R, double I) {
        RealPart = R;
        ImagePart = I;
    }

    //Override Object
    public boolean equals(Object obj) {
        if (this == obj) {   // 是否引用同一个对象
            return true;
        }
        if (obj == null) {   // 是否为空
            return false;
        }
        if (getClass() != obj.getClass()) {  // 是否属于同一个类型
            return false;
        }
        Complex other = (Complex) obj;
        if (other.RealPart == RealPart && other.ImagePart == ImagePart) {
            return true;
        } else {
            return false;
        }
    }

    public String toString() {
        if (ImagePart < 0) {
            return RealPart + "-" + -ImagePart + "i";
        } else {
            return RealPart + "+" + ImagePart + "i";
        }
    }

    // 定义公有方法:加减乘除
    Complex ComplexAdd(Complex a) {
        Complex result = new Complex();
        result.RealPart = RealPart + a.RealPart;
        result.ImagePart = ImagePart + a.ImagePart;
        return result;
    }

    Complex ComplexSub(Complex a) {
        Complex result = new Complex();
        result.RealPart = RealPart - a.RealPart;
        result.ImagePart = ImagePart - a.ImagePart;
        return result;
    }

    Complex ComplexMulti(Complex a) {
        Complex result = new Complex();
        result.RealPart = RealPart * a.RealPart - ImagePart * a.ImagePart;
        result.ImagePart = ImagePart * a.RealPart + RealPart * a.ImagePart;
        return result;
    }

    Complex ComplexDiv(Complex a) {
        Complex result = new Complex();
        result.RealPart = (RealPart * a.RealPart + ImagePart * a.ImagePart) / (a.RealPart * a.RealPart + a.ImagePart * a.ImagePart);
        result.ImagePart = (ImagePart * a.RealPart - RealPart * a.ImagePart) / (a.RealPart * a.RealPart + a.ImagePart * a.ImagePart);
        return result;
    }
}
