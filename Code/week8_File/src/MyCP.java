import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class MyCP {


    public static void main(String[] args) {
        String mode = args[0];
        if (mode.equals("-tx")) {
            Path pathtxt = Paths.get(args[1]);
            Path pathbin = Paths.get(args[2]);
            txt2bina(pathtxt, pathbin);
        } else if (mode.equals("-xt")) {
            Path pathtxt = Paths.get(args[2]);
            Path pathbin = Paths.get(args[1]);
            String line;
            line = txt2String(pathbin.toFile());
            System.out.println(line);

        } else {
            System.out.println("wrong");
        }

    }

    public static void txt2bina(Path txt, Path bin) {
        String line;
        try (FileReader reader = new FileReader(txt.toFile()); OutputStream outputStream = Files.newOutputStream(bin,
                StandardOpenOption.CREATE, StandardOpenOption.APPEND);
             BufferedReader br = new BufferedReader(reader); BufferedOutputStream buffered = new BufferedOutputStream(outputStream) // 建立一个对象，它把文件内容转成计算机能读懂的语言
        ) {
            while ((line = br.readLine()) != null) {
                outputStream.write(decimalToHex(java.lang.Integer.parseInt(line)).getBytes());
                int valueTen2 = Integer.parseInt(decimalToHex(java.lang.Integer.parseInt(line)), 16);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String decimalToHex(int decimal) {
        String hex = "";
        while (decimal != 0) {
            int hexValue = decimal % 16;
            hex = toHexChar(hexValue) + hex;
            decimal = decimal / 16;
        }
        return hex;
    }

    public static char toHexChar(int hexValue) {
        if (hexValue <= 9 && hexValue >= 0)
            return (char) (hexValue + '0');
        else
            return (char) (hexValue - 10 + 'A');
    }

    public static String txt2String(File file) {
        StringBuilder result = new StringBuilder();
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String s = null;
            while ((s = br.readLine()) != null) {
                result.append(System.lineSeparator() + s);
            }
            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result.toString();
    }
}