import java.io.*;
public class MyOD{

    public static void main(String[] args) throws IOException {
        String file = args[0];
        try (FileInputStream input = new FileInputStream(file)) {
            byte[] data = new byte[1024];
            int i, flag;
            input.read(data);
            for (i = 0; i < 1024; i = i + 4) {
                if (i % 16 == 0) {
                    System.out.printf("\n%07o\t\t", i );
                }  
                System.out.printf(" %02x%02x%02x%02x\t", data[i + 3], data[i + 2], data[i + 1], data[i]);
                if ((i + 4) % 16 == 0) {
                    System.out.println();
                    System.out.printf("\t  ");
                    for (int j = i - 12; j < i+4 ; j++) {
                        if ((int) data[j] == 10) {
                            System.out.printf(" \\");
                            System.out.printf("n ");
                        } else {
                            System.out.printf("  %c ", data[j]);
                        }
                    }
                }
                if (data[i+4] ==0 ) {
                    System.out.println();
                    for (int j = i-i%16; data[j] != 0; j++) {
                        if ((int) data[j] == 10) {
                            System.out.printf(" \\");
                            System.out.printf("n  ");
                        }
                    }
                    break;
                }
            }
            System.out.printf("%07o", i+3 );
        }
    }
}