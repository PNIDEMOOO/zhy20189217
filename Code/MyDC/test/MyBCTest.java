import java.util.Scanner;

/*
 * 中缀表达式转为后缀表达式
 */
public class MyBCTest {
    public static void main(String[] args) throws Exception {

        String input, again;
        try {
            Scanner in = new Scanner(System.in);
            do {
                MyDC evaluator = new MyDC();
                System.out.println("Enter a valid infix expression: ");
                input = in.nextLine();

                MyDC mydc = new MyDC();
                System.out.println(MyBC.infixToSuffix(input));
                System.out.println(mydc.evaluate(MyBC.infixToSuffix(input)));

                System.out.print("Evaluate another expression [Y/N]? ");
                again = in.nextLine();
                System.out.println();
            }
            while (again.equalsIgnoreCase("y"));
        } catch (Exception IOException) {
            System.out.println("Input exception reported");
        }

    }
}