package com.example.week6_test5;
import android.content.Context;

public class CellView extends android.support.v7.widget.AppCompatImageView {
    int x;
    int y;

    public CellView(Context context, int x, int y) {
        super(context);
        this.x = x;
        this.y = y;
    }
}