// Server Classes
abstract class Data {
    abstract public void DisplayValue();
}
class Integer extends  Data {
    int value;
    Integer() {
        value=100;
    }
    public void DisplayValue(){
        System.out.println (value);
    }
}
class Double extends Data{
    double value;
    Double(){
        value = 4.9*10E-324;
    }
    public void DisplayValue(){
        System.out.println(value);
    }
}
// Pattern Classes
abstract class Factory {
    abstract public Data CreateDataObject();
}
class IntFactory extends Factory {
    public Data CreateDataObject(){
        return new Integer();
    }
}
class DoubleFactory extends Factory {
    public Data CreateDataObject(){
        return new Double();
    }
}
//Client classes
class Document {
    Data pd;
    Document(Factory pf){
        pd = pf.CreateDataObject();
    }
    public void DisplayData(){
        pd.DisplayValue();
    }
}
//Test class
public class MyDoc {
    static Document d;
    static Document d1;
    public static void main(String[] args) {
        d = new Document(new IntFactory());
        d.DisplayData();

        d1 = new Document(new DoubleFactory());
        d1.DisplayData();
    }
}