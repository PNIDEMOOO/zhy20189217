import java.util.*;
public class MySort {
    public static void main(String[] args) {
        int field = Integer.parseInt(args[0])-1;
        String[] toSort = {"aaa:10:1:1",
                "ccc:30:3:4",
                "bbb:50:4:5",
                "ddd:20:5:3",
                "eee:40:2:20"};
        System.out.println("Before sort:");
        for (String str : toSort) {
            System.out.println(str);
        }

        int[] newArray = new int[toSort.length];
        for (int i = 0; i < toSort.length; i++) {//遍历toSort这个数组，将每个字符串按照冒号进行分隔，分隔后得到的字符串保留在tmp中
            String[] tmp = toSort[i].split(":");
            newArray[i] = Integer.parseInt(tmp[field]);//求field的所对应的值
        }
        Arrays.sort(newArray);//对newArray进行升序排列，要求按照第二个field进行排序，所以上面newArray应该保留的是第二个field的值
        System.out.println("After sort:");

        for (int i = 0; i < newArray.length; i++) {//升序输出以field为关键字的toSort排序，所以下面就是寻找newArray[i]相等的关键字所在的字符串位置
            for (int j = 0; j < toSort.length; j++){
                if (newArray[i] ==Integer.parseInt((toSort[j].split(":"))[field])) {//当newArray[i]等于temp[field]里面的数值时，输出,
                    // 但是由于temp是一个一直变化的，所以只能重新进行temp数组的生成过程
                    System.out.println(toSort[j]);
                }
            }
        }
    }
}