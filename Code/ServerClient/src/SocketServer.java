import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Base64;
import java.util.Map;

public class SocketServer {
    //搭建服务器端

    private static byte[] severtemp3;


    //甲方公钥
    private static byte[] publicKey1;
    //甲方私钥
    private static byte[] privateKey1;

    private static byte[] publicKey2;
    //乙方公钥

//甲方本地密钥
    private static byte[] key1;



    private static final void initKey() throws Exception {
        //生成甲方密钥对
        Map<String, Object> keyMap1 = DHCoder.initKey();
        publicKey1 = DHCoder.getPublicKey(keyMap1);
        privateKey1 = DHCoder.getPrivateKey(keyMap1);
        System.out.println("甲方公钥:\n" + Base64.getEncoder().encodeToString(publicKey1));
        System.out.println("甲方私钥:\n" + Base64.getEncoder().encodeToString(privateKey1));

        FileOutputStream f = new FileOutputStream("severpublickey.dat");
        ObjectOutputStream b = new ObjectOutputStream(f);
        b.writeObject(publicKey1);

        FileOutputStream f3 = new FileOutputStream("severprivatekey.dat");
        ObjectOutputStream b3 = new ObjectOutputStream(f3);
        b.writeObject(privateKey1);


    }


    public static void main(String[] args) throws Exception {
        SocketServer socketService = new SocketServer();
        //1、a)创建一个服务器端Socket，即SocketService
        initKey();


        socketService.oneServer();
    }

    public void oneServer() {
        try {
            ServerSocket server = null;
            try {
                server = new ServerSocket(12306);
                //b)指定绑定的端口，并监听此端口。
                System.out.println("服务器启动成功");
                //创建一个ServerSocket在端口5209监听客户请求
            } catch (Exception e) {
                System.out.println("没有启动监听：" + e);
                //出错，打印出错信息
            }
            File file = new File("lee".hashCode()+"clientpublickey.dat");
            while (!file.exists()) {
                System.out.println("等待中......");
            }
            getkey ClientprivateKey = new getkey("lee".hashCode()+"clientpublickey.dat");
            publicKey2 = ClientprivateKey.readfrombytedat();
            key1 = DHCoder.getSecretKey(publicKey2, privateKey1);
            System.out.println("甲方本地密钥:\n" + Base64.getEncoder().encodeToString(key1));


            Socket socket = null;

            try {
                socket = server.accept();
                //2、调用accept()方法开始监听，等待客户端的连接
                //使用accept()阻塞等待客户请求，有客户
                //请求到来则产生一个Socket对象，并继续执行
            } catch (Exception e) {
                System.out.println("Error." + e);
                //出错，打印出错信息
            }
            //3、获取输入流，并读取客户端信息
            String line;
            String postfix;
            int result;
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            //由Socket对象得到输入流，并构造相应的BufferedReader对象
            PrintWriter writer = new PrintWriter(socket.getOutputStream());
            //由Socket对象得到输出流，并构造PrintWriter对象
            BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            //由Socket对象构造BufferedReader对象

            String input = in.readLine();
            System.out.println("接收到的客户端后缀表达式为:" + input);
            //在标准输出上打印从客户端读入的字符串
//            line=in.readLine();
            //从标准输入读入一字符串
            //4、获取输出流，响应客户端的请求
            while (!input.equals("end")) {
                //如果该字符串为 "end"，则停止循环
                MyBC mybc = new MyBC();
                MyDC mydc = new MyDC();
                //创建计算类对象evaluator，计算客户端发送的后缀表达式
                postfix = mybc.infixToSuffix(input);
                result = mydc.evaluate(postfix);
                writer.println(result);
                //向客户端输出该字符串
                writer.flush();
                //刷新输出流，使Client马上收到该字符串
                System.out.println("服务器计算得到的后缀表达式计算结果为:" + result);
                //在系统标准输出上打印读入的字符串
                System.out.println("客户端:" + in.readLine());
                //从Client读入一字符串，并打印到标准输出上
//                line=br.readLine();
                line = in.readLine();
                //从系统标准输入读入一字符串
            } //继续循环

            //5、关闭资源
            writer.close(); //关闭Socket输出流
            in.close(); //关闭Socket输入流
            socket.close(); //关闭Socket
            server.close(); //关闭ServerSocket
        } catch (Exception e) {//出错，打印出错信息
            System.out.println("Error." + e);
        }
    }
}