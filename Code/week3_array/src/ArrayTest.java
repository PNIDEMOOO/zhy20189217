public class ArrayTest {
    public static void main(String[] args) {
        //定义一个数组
        int arr[] = {1, 2, 3, 4, 5, 6, 7, 8};
        //打印原始数组的值
        for (int i : arr) {
            System.out.print(i + " ");
        }
        System.out.println();
        // 添加代码删除上面数组中的5

        for (int i = 4; i < arr.length - 1; i++) {
            arr[i] = arr[i + 1];
        }
        arr[arr.length - 1] = 0;

        //打印出 1 2 3 4 6 7 8 0
        for (int i : arr) {
            System.out.print(i + " ");
        }
        System.out.println();
        // 添加代码再在4后面5

        for (int i = arr.length - 1; i > 4; i--) {
            arr[i] = arr[i - 1];
        }
        arr[4] = 5;

        //打印出 1 2 3 4 5 6 7 8
        for (int i : arr) {
            System.out.print(i + " ");
        }
        System.out.println();
    }
}
