public class Score {

    private int No;          //学号
    private int score;        //成绩

    //无参数的构造方法
    public Score() {

        No = 1000;
        score = 0;
    }

    public Score(int n, int s) {     //有两个参数的构造方法
        No = n;
        score = s;
    }

    public void setInfo(int n, int s) {      //设置成绩
        No = n;
        score = s;
    }

    public int getNo() {
        return No;     //获取学号
    }

    public int getScore() {
        return score;    //获取成绩
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {   // 是否引用同一个对象
            return true;
        }
        if (obj == null) {   // 是否为空
            return false;
        }
        if (getClass() != obj.getClass()) {  // 是否属于同一个类型
            return false;
        }
        Score other = (Score) obj;
        if (other.No == No && other.score == score) {
            return true;
        } else {
            return false;
        }
    }

    @java.lang.Override
    public java.lang.String toString() {
        return "Score{" +
                "No=" + No +
                ", score=" + score +
                '}';
    }
    //    @Override
//    public String toString() {
//        return No + "\t" + score;
//    }

    public static void main(String[] args) {
        Score s1 = new Score(3, 66);
        System.out.println(s1.toString());


        Score s2 = new Score(3, 4);

        printEverything(s1);
        printEverything(s2);
    }

    private static void printEverything(Score s) {

        System.out.println(s.toString());
        System.out.println(s.getClass());
        System.out.println(s.getClass().getName());
    }
}