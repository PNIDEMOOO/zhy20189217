public class Grade {
    private int No;          //学号
    private int grade;        //年级

    //无参数的构造方法
    public Grade() {

        No = 1000;
        grade = 1;
    }

    public Grade(int n, int s) {     //有两个参数的构造方法
        No = n;
        grade = s;
    }
    public void setInfo(int n, int s) {      //设置年级
        No = n;
        grade = s;
    }

    public int getNo() {
        return No;     //获取学号
    }

    public int getScore() {
        return grade;    //获取年级
    }
}
