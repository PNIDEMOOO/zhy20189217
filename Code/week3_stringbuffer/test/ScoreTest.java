import junit.framework.TestCase;
import org.junit.Test;

public class ScoreTest extends TestCase {
    Score score0 = new Score(1,99);
    Score score1 = new Score(1,99);
    Score score2 = new Score(2,98);
    Grade grade1 = new Grade(1,1);
    Score score3;
    @Test
    public void testExceptions(){
        assertEquals(true,score0.equals(score1));//引用同一个对象
        assertEquals(false,score0.equals(score2));//二者对应数值不相等
        assertEquals(false,score0.equals(grade1));//不是同样类型的object
        assertEquals(false,score0.equals(score3));//object为空
    }

}