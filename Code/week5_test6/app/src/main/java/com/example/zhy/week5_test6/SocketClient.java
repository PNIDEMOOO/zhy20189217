package com.example.zhy.week5_test6;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.time.Instant;
import java.util.Base64;
import java.util.Map;

public class SocketClient {
    // 搭建客户端
    private static byte [] severtemp;
    private static byte[] publicKey1 ;
    //乙方公钥
    private static byte[] publicKey2;
    //乙方私钥
    private static byte[] privateKey2;
    //乙方本地密钥
    private static byte[] key2;

    //对方公钥
    public SocketClient() throws Exception {

    }

    public static final void initKey() throws Exception{
        getkey ServerpulicKey = new getkey("severpublickey.dat");
        severtemp = ServerpulicKey.readfrombytedat();
        publicKey1 = severtemp;
        System.out.println("甲方公钥:\n" + Base64.getEncoder().encodeToString(publicKey1));
       Map<String, Object> keyMap2 = DHCoder.initKey(publicKey1);
        publicKey2 = DHCoder.getPublicKey(keyMap2);
        privateKey2 = DHCoder.getPrivateKey(keyMap2);
        System.out.println("乙方公钥:\n" + Base64.getEncoder().encodeToString(publicKey2));
        System.out.println("乙方私钥:\n" + Base64.getEncoder().encodeToString(privateKey2));
        Instant now =Instant.now();
        FileOutputStream f = new FileOutputStream(now.hashCode()+"clientpublickey.dat");
        ObjectOutputStream b = new ObjectOutputStream(f);
        b.writeObject(publicKey2);

        key2 = DHCoder.getSecretKey(publicKey1, privateKey2);
        System.out.println("乙方本地密钥:\n" + Base64.getEncoder().encodeToString(key2));

    }


    public static void main(String[] args) throws Exception {
        initKey();
        try {
            String host = "127.0.0.1";
            int port = 55533;
            // 1、创建客户端Socket，指定服务器地址和端口
            Socket socket = new Socket(host, port);
            //Socket socket = new Socket("192.168.1.115", 5209);
            System.out.println("客户端启动成功");
            // 2、获取输出流，向服务器端发送信息
            // 向本机的5200端口发出客户请求
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            // 由系统标准输入设备构造BufferedReader对象
            PrintWriter write = new PrintWriter(socket.getOutputStream());
            // 由Socket对象得到输出流，并构造PrintWriter对象
            //3、获取输入流，并读取服务器端的响应信息
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            // 由Socket对象得到输入流，并构造相应的BufferedReader对象
            String readline;
            String postfix;
            readline = br.readLine(); // 从系统标准输入读入一字符串
            while (!readline.equals("end")) {
                // 若从标准输入读入的字符串为 "end"则停止循环
                postfix = MyBC.infixToSuffix(readline);
                write.println(postfix);
                // 将从系统标准输入读入的字符串输出到Server
                write.flush();
                // 刷新输出流，使Server马上收到该字符串
                System.out.println("客户端转换的后缀表达式为:" + postfix);
                // 在系统标准输出上打印读入的字符串
                System.out.println("服务端传来的后缀表达式计算结果为:" + in.readLine());
                // 从Server读入一字符串，并打印到标准输出上
                readline = br.readLine(); // 从系统标准输入读入一字符串
            } // 继续循环
            //4、关闭资源
            write.close(); // 关闭Socket输出流
            in.close(); // 关闭Socket输入流
            socket.close(); // 关闭Socket
        } catch (Exception e) {
            System.out.println("can not listen to:" + e);// 出错，打印出错信息
        }
    }

}