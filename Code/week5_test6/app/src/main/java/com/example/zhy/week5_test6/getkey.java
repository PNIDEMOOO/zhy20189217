package com.example.zhy.week5_test6;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.security.Key;

public class getkey {
    String fileName;
    static byte [] k;
    public getkey(String fileName) {

       this.fileName = fileName;
    }
    public Key readfromkeydat() throws  Exception {
        FileInputStream f = new FileInputStream(fileName);
        ObjectInputStream b = new ObjectInputStream(f);
        Key k = (Key) b.readObject();
        return k;
    }
    public byte[] readfrombytedat() throws  Exception {
        FileInputStream f = new FileInputStream(fileName);
        ObjectInputStream b = new ObjectInputStream(f);
        k = (byte[])b.readObject();

        f.close();
        b.close();
        return k;
    }
}
