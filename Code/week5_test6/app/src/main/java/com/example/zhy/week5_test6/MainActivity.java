package com.example.zhy.week5_test6;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    private Button button;

    private EditText editText;

    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button = (Button) findViewById(R.id.btn1);
        editText = (EditText) findViewById(R.id.editText);
        textView = (TextView) findViewById(R.id.textView3);
//        button.setOnClickListener((View.OnClickListener) this);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.btn1:
                        String infix = editText.getText().toString();
                        MyBC mybc = new MyBC();
                        String suffix = mybc.infixToSuffix(infix);
                        textView.setText(suffix);
                        break;
                    default:
                        break;

                }
            }
        });

    }
}

