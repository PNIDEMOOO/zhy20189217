import java.util.Arrays;


public class TestArgs2 {
    public static void main(String[] args) {

        int sum = 0;
        int[] a = new int[args.length + 1];
        for (int i = 0; i < a.length-1; i++) {
            a[i] = Integer.parseInt(args[i]);
        }


        sum = clSum(a);
        System.out.println(sum);
    }

    //递归函数
    public static int clSum(int[] arr) {
        int j = arr.length;
        if (j == 1) {
            return arr[0];
        } else {
            int[] b = Arrays.copyOf(arr, arr.length - 1);
            return clSum(b) + arr[j - 1];
        }
    }
}
