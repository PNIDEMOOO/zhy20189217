public class UserStat {
    int userCount=10;

    public int getUserCount() {
        return userCount;
    }

    public void increment() {
        userCount++;
    }

    public void decrement() {
        userCount--;
    }

    public static void main(String[] args) {

    }
}